openerp.bt_message_history_chatter = function(openerp) {
    var _t = openerp.web._t;
    var initial_mode = "view"
    var QWeb = openerp.web.qweb;

    openerp.mail.ThreadComposeMessage.include({
        bind_events: function () {
            var self = this;
            this._super();
            // header icons bindings
	    this.$el.find('.oe_send_message').on('click', this.on_send_message); 
            this.$el.find('.oe_attach_history').on('click', this.on_attach_history);
            this.$el.find('.oe_attach_history_noattachment').on('click', this.on_attach_history_noattachment);
        },

	on_send_message: function (default_composition_mode) {
            var self = this
            messages = []
            attachment_ids = []
            var msg = false
            var values = new openerp.web.Model('mail.message').call('action_send_message', [[this.id], self.context])
            
            values.done(function(vals) {
                action = {
                    'type': 'ir.actions.act_window',
                    'res_model': 'mail.compose.message',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'views': [[false, 'form']],
                    'target': 'new',
                    'context' : vals
                }
                self.do_action(action);
                self.on_cancel();
            })
        },

        on_attach_history: function (default_composition_mode) {
            var self = this
            messages = []
            attachment_ids = []
            var msg = false
            var values = new openerp.web.Model('mail.message').call('attach_history', [[this.id], self.context])
            
            values.done(function(vals) {
                action = {
                    'type': 'ir.actions.act_window',
                    'res_model': 'mail.compose.message',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'views': [[false, 'form']],
                    'target': 'new',
                    'context' : vals
                }
                self.do_action(action);
                self.on_cancel();
            })
        },

	on_attach_history_noattachment: function (default_composition_mode) {
            var self = this
            messages = []
            attachment_ids = []
            var msg = false
            var values = new openerp.web.Model('mail.message').call('attach_history_noattachments', [[this.id], self.context])
            
            values.done(function(vals) {
                action = {
                    'type': 'ir.actions.act_window',
                    'res_model': 'mail.compose.message',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'views': [[false, 'form']],
                    'target': 'new',
                    'context' : vals
                }
                self.do_action(action);
                self.on_cancel();
            })
        },
        
        
    })
};
