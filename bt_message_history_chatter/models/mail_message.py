# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2017 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

_logger = logging.getLogger(__name__)

from openerp.osv import osv, fields
from openerp import api
from openerp import tools, SUPERUSER_ID
from openerp.tools.translate import _

class mail_message(osv.Model):
    _inherit = 'mail.message'
    
    _columns = {
        'sent_history': fields.boolean('Sent History/Attachments'),
    }
    
    @api.cr_uid_context
    def action_send_message(self, cr, uid, ids, context=None):
        vals = context or {}
        subject = ''
        recipients_list = []
        if 'default_model' in context and 'default_res_id' in context and context['default_model'] and \
                            context['default_res_id']:
            recipients = self.pool.get(context['default_model']).message_get_suggested_recipients(cr, uid, [context['default_res_id']], context=context)
            for k, v in recipients.items():
                for recipient in v:
                    recipients_list.append(recipient[0])
            
        vals.update({
            'default_partner_ids': recipients_list,
        })
        return dict(vals)
    
    def action_attach_history(self, cr, uid, ids, check_attachment, context=None):  
        vals = context or {}
        attachment_pool = self.pool.get('ir.attachment')
        signature = self.pool.get('res.users').browse(cr, uid, uid, context=context).signature
        msg = '<br/><br/>' 
        if signature: 
            msg += signature
        msg += '<br/><br/><br/><div style="border-left:1px black;margin-left:2%;">----- Message History ----- <br/>'
        subject = ''
        attachments = []
        recipients_list = []
        if 'default_model' in context and 'default_res_id' in context and context['default_model'] and \
                            context['default_res_id']:
            ctx = context.copy()
            ctx.update({'from_send_answer': True})
            recipients = self.pool.get(context['default_model']).message_get_suggested_recipients(cr, uid, [context['default_res_id']], context=ctx)
            for k, v in recipients.items():
                for recipient in v:
                    recipients_list.append(recipient[0])
            
            msg_ids = self.pool.get('mail.message').search(cr, uid, [('model', '=', context['default_model']), 
                          ('type', '!=', 'notification'),
                          ('res_id', '=', context['default_res_id'])], order='id', context=context)
            issue = self.pool.get(context['default_model']).browse(cr, uid, context['default_res_id'], context=context)
            subject = issue.name or '' + ': Sent History'
            for msg_id in msg_ids:
                msg_obj = self.pool.get('mail.message').browse(cr, uid, msg_id, context=context)
                author = msg_obj.author_id and msg_obj.author_id.name or msg_obj.email_from
                partners = ''
                if msg_obj.notified_partner_ids:
                    for partner in msg_obj.notified_partner_ids:
                        partners += partner.name + ', '
                    partners = ' to ' + partners
                else:
                    partners = ' updated document '
                if msg_obj.subject:
                    msg += '<b>' + msg_obj.subject + '</b>'
                msg_body = '<br/>'
                if msg_obj.body:
                    msg_body = msg_obj.body
                    if '<p>' in msg_body:
                        msg_body = msg_body.replace('<p>','<br/>')
                    if '</p>' in msg_body:
                        msg_body = msg_body.replace('</p>','<br/>')
                msg += msg_body + '<i>' + author + partners + '.' + msg_obj.date + '</i><br/><br/>'
            if check_attachment:
                attachment_ids = attachment_pool.search(cr, uid, [('res_model', '=', context['default_model']),
                    ('res_id', '=', context['default_res_id'])], order='id', context=context)
                if attachment_ids:
                    if attachments:
                        attachment_ids += attachments
                    vals.update({'default_attachment_ids' : [(6, 0, attachment_ids)]})
        vals.update({
            'default_body': msg,
            'default_subject': tools.ustr(subject),
            'default_sent_history': True,
            'default_partner_ids': recipients_list,
        })
        return dict(vals)
    
    @api.cr_uid_context
    def attach_history(self, cr, uid, ids, context=None):
        result = self.action_attach_history(cr, uid, ids, check_attachment=True, context=context)
        return result
        
    @api.cr_uid_context
    def attach_history_noattachments(self, cr, uid, ids, context=None):
        result = self.action_attach_history(cr, uid, ids, check_attachment=False, context=context)
        return result
    
    
class mail_notification(osv.Model):
    _inherit = 'mail.notification'    
      
    def get_signature_footer(self, cr, uid, user_id, res_model=None, res_id=None, context=None, user_signature=True):
        """ Format a standard footer for notification emails (such as pushed messages
            notification or invite emails).
            Format:
                <p>--<br />
                    Administrator
                </p>
                <div>
                    <small>Sent from <a ...>Your Company</a> using <a ...>OpenERP</a>.</small>
                </div>
        """
        footer = ""
        if not user_id:
            return footer
  
        # add user signature
        user = self.pool.get("res.users").browse(cr, SUPERUSER_ID, [user_id], context=context)[0]
        if 'default_sent_history' not in context or not context['default_sent_history']:
            if user_signature:
                if user.signature:
                    signature = user.signature
                else:
                    signature = "--<br />%s" % user.name
                footer = tools.append_content_to_html(footer, signature, plaintext=False)
  
        # add company signature
        if user.company_id.website:
            website_url = ('http://%s' % user.company_id.website) if not user.company_id.website.lower().startswith(('http:', 'https:')) \
                else user.company_id.website
            company = "<a style='color:inherit' href='%s'>%s</a>" % (website_url, user.company_id.name)
        else:
            company = user.company_id.name
        sent_by = _('Sent by %(company)s using %(odoo)s')
  
        signature_company = '<br /><small>%s</small>' % (sent_by % {
            'company': company,
            'odoo': "<a style='color:inherit' href='https://www.odoo.com/'>Odoo</a>"
        })
        footer = tools.append_content_to_html(footer, signature_company, plaintext=False, container_tag='div')
        return footer
     
 
# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:   
    