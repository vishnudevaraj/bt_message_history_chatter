# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2017 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Message history in chatter emails',
    'version': '0.1',
    'category': 'custom',
    'summary': 'Chatter as history attachments',
    'description': """
    The module introduces two additional messaging options in the chatter area of Odoo forms. "Send a message" and "Log an Internal Note" are the messaging options available by default. The new options "Answer" and "Answer without attachment" allows user to send messages by including the entire history of communications associated with the record, just like a regular email thread which quotes previous communication history at the bottom.
""",
    'author' : 'BroadTech IT Solutions Pvt Ltd',
    'website' : 'http://www.broadtech-innovations.com',
    'depends': ['mail'],
    'images': [
    ],
    'data': [
        'view/templates.xml',
        'view/mail_message_view.xml',
    ],
    'qweb': [
        'static/src/xml/mail.xml',
    ],
    'images': ['static/description/banner.jpg'],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2: